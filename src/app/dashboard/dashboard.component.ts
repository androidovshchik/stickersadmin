import {Component} from '@angular/core';
import {Category, InfoApp, StickerPack} from '../app.models';
import {CategoriesService} from '../firestore/categories.service';
import {AngularFirestore} from '@angular/fire/firestore';
import {StickerPackService} from '../firestore/sticker-pack.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent {

  menuPosition = 1;

  infoApp: InfoApp = {
    botToken: null,
    proxyHost: null,
    proxyPort: null,
    proxyUsername: null,
    proxyPassword: null,
    stickersBackupDate: null
  };

  categories: Category[] = [];

  stickersPack: StickerPack[] = [];

  constructor(public categoriesService: CategoriesService, public store: AngularFirestore, public stickersPackService: StickerPackService) {
    const subscription1 = categoriesService._items.asObservable()
      .subscribe((model: Category) => {
        if (model != null) {
          this.categories.push(model);
        } else {
          this.sortCategories();
          if (subscription1) {
            subscription1.unsubscribe();
          }
        }
      });
    const subscription2 = store.doc<InfoApp>('info/app')
      .valueChanges()
      .first()
      .subscribe((value) => {
        console.log(value);
        if (value) {
          this.infoApp = value;
          localStorage.setItem('bot_token', this.infoApp.botToken);
        }
        if (subscription2) {
          subscription2.unsubscribe()
        }
      });
    const subscription3 = stickersPackService._items.asObservable()
      .subscribe((model: StickerPack) => {
        if (model != null) {
          this.stickersPack.push(model);
        } else {
          this.sortStickersPack();
          if (subscription3) {
            subscription3.unsubscribe();
          }
        }
      });
  }

  sortCategories() {
    this.categories.sort((category1, category2) => {
      return category1.title.toLowerCase().localeCompare(category2.title.toLowerCase());
    });
  }

  sortStickersPack() {
    this.stickersPack.sort((stickersPack1, stickersPack2) => {
      return stickersPack1.title.toLowerCase().localeCompare(stickersPack2.title.toLowerCase());
    });
  }
}
