import {Component, Input} from '@angular/core';
import {Category, Sticker, StickerPack} from '../app.models';
import {StickersApiService} from './stickers-api.service';
import {FirestoreComponent} from '../firestore/firestore.component';
import {AngularFirestore} from '@angular/fire/firestore';

@Component({
  selector: 'app-sticker-packs',
  templateUrl: './stickers.component.html',
  styleUrls: ['./stickers.component.css']
})
export class StickersComponent extends FirestoreComponent {

  @Input()
  categories: Category[] = [];

  @Input()
  stickersPack: StickerPack[] = [];

  currentPack = 0;

  errorMessage = "";

  constructor(public store: AngularFirestore, public stickersApiService: StickersApiService) {
    super(store);
    stickersApiService._errors.asObservable()
      .subscribe((error: any) => {
        if (error.status == 400) {
          this.errorMessage = '[Не найден]';
        } else if (error.status == 0) {
          this.errorMessage = '[Требуется прокси]';
        } else {
          this.errorMessage = `[Неизвестная ошибка ${error.status}]`;
        }
      });
  }

  saveStickerPack(target) {
    if (!target.value.trim()) {
      target.value = "";
      return
    }
    target.disabled = true;
    const stickerPack: StickerPack = {
      title: target.value.trim(),
      titleEn: null,
      category: "",
      preview: "",
      stickerSet: null,
      keywords: []
    };
    const uid = StickersComponent.generateUid();
    this.updateDocument('stickers-pack', uid, stickerPack, false)
      .then(() => {
        stickerPack._uid = uid;
        this.stickersPack.push(stickerPack);
        this.sortStickersPack();
        this.currentPack = this.stickersPack.indexOf(stickerPack);
        target.value = "";
        target.disabled = false;
        target.focus();
      })
      .catch(error => {
        console.log(error);
        target.disabled = false;
      });
  }

  /*updateStickerPack(target) {
    target.disabled = true;
    const stickerPack = JSON.parse(JSON.stringify(this.stickersPack[this.currentPack]));
    delete stickerPack._uid;
    if (stickerPack.stickerSet) {
      delete stickerPack.stickerSet.title;
      delete stickerPack.stickerSet.contains_masks;
      stickerPack.stickerSet.stickers.forEach((sticker: Sticker) => {
        if (sticker.file_size) {
          delete sticker.file_size;
        }
        delete sticker.width;
        delete sticker.height;
        if (sticker.thumb) {
          if (sticker.thumb.file_size) {
            delete sticker.thumb.file_size;
          }
          delete sticker.thumb.width;
          delete sticker.thumb.height;
        }
        if (sticker.set_name) {
          delete sticker.set_name;
        }
        if (sticker.mask_position) {
          delete sticker.mask_position;
        }
      });
    }
    target.innerText = 'Подождите...';
    this.updateDocument('stickers-pack', this.stickersPack[this.currentPack]._uid, stickerPack, false)
      .then(() => {
        target.innerText = 'Сохранено';
        setTimeout(() => {
          target.innerText = 'Сохранить';
          target.disabled = false;
        }, 1000);
      })
      .catch(error => {
        console.log(error);
        target.innerText = 'Не сохранено';
        setTimeout(() => {
          target.innerText = 'Сохранить';
          target.disabled = false;
        }, 1000);
      });
  }*/

  deleteStickerPack(i, stickerPack) {
    this.deleteDocument('stickers-pack', stickerPack._uid)
      .then(() => {
        this.currentPack = i - 1 < 0 ? 0 : i - 1;
        this.stickersPack.splice(i, 1)
      })
      .catch(error => {
        console.log(error);
        alert('Не удалось удалить стикерпак');
      });
  }

  sortStickersPack() {
    this.stickersPack.sort((stickersPack1, stickersPack2) => {
      return stickersPack1.title.toLowerCase().localeCompare(stickersPack2.title.toLowerCase());
    });
  }

  onSelectSticker(i) {
    this.currentPack = i;
    this.errorMessage = "";
  }

  searchStickers(target) {
    this.errorMessage = "";
    if (!target.value.trim()) {
      target.value = "";
      return
    }
    this.stickersApiService.getStickerSet(target.value)
      .subscribe((value: any) => {
        if (value != null) {
          this.stickersPack[this.currentPack].stickerSet = value.result;
          delete this.stickersPack[this.currentPack].stickerSet.title;
          delete this.stickersPack[this.currentPack].stickerSet.contains_masks;
          this.stickersPack[this.currentPack].stickerSet.stickers.forEach((sticker: Sticker) => {
            if (sticker.file_size) {
              delete sticker.file_size;
            }
            delete sticker.width;
            delete sticker.height;
            if (sticker.thumb) {
              if (sticker.thumb.file_size) {
                delete sticker.thumb.file_size;
              }
              delete sticker.thumb.width;
              delete sticker.thumb.height;
            }
            if (sticker.set_name) {
              delete sticker.set_name;
            }
            if (sticker.mask_position) {
              delete sticker.mask_position;
            }
          });
        }
      })
  }
}
