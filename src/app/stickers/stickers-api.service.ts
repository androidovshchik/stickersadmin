import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of, Subject} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Sticker, StickerSet} from '../app.models';

@Injectable({
  providedIn: 'root'
})
export class StickersApiService {

  _errors = new Subject<any>();

  constructor(private http: HttpClient) {}

  getStickerSet(name: string): Observable<StickerSet> {
    const token = localStorage.getItem('bot_token');
    return this.http.get(`https://api.telegram.org/bot${token}/getStickerSet?name=${encodeURIComponent(name)}`).pipe(
      catchError(error => {
        this._errors.next(error);
        return of(null);
      })
    );
  }

  getFile(sticker: Sticker): Observable<any> {
    const token = localStorage.getItem('bot_token');
    const fileId = sticker.thumb && sticker.thumb.file_id ? sticker.thumb.file_id : sticker.file_id;
    return this.http.get(`https://api.telegram.org/bot${token}/getFile?file_id=${encodeURIComponent(fileId)}`).pipe(
      catchError(() => {
        return of(null);
      })
    );
  }

  downloadFile(path: string): Observable<any> {
    const token = localStorage.getItem('bot_token');
    return this.http.get(`https://api.telegram.org/file/bot${token}/${encodeURIComponent(path)}`, {
      responseType: 'blob'
    }).pipe(
      catchError(() => {
        return of(null);
      })
    );
  }
}
