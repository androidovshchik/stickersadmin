import {Component, ElementRef, Input, ViewChild} from '@angular/core';
import {InfoApp, StickerPack} from '../app.models';
import {catchError, finalize, mergeMap} from 'rxjs/operators';
import {of} from 'rxjs';
import {StickersApiService} from '../stickers/stickers-api.service';
import {FirestoreComponent} from '../firestore/firestore.component';
import {AngularFirestore} from '@angular/fire/firestore';
import {AngularFireStorage} from '@angular/fire/storage';

@Component({
  selector: 'app-backup',
  templateUrl: './backup.component.html',
  styleUrls: ['./backup.component.css']
})
export class BackupComponent extends FirestoreComponent {

  @ViewChild('update', {static: true})
  update : ElementRef;

  @Input()
  infoApp: InfoApp = {
    botToken: null,
    proxyHost: null,
    proxyPort: null,
    proxyUsername: null,
    proxyPassword: null,
    stickersBackupDate: null
  };

  @Input()
  stickersPack: StickerPack[] = [];

  logs: string[] = [];

  cancel = false;

  successCount = 0;

  failCount = 0;

  reset = false;

  constructor(private storage: AngularFireStorage, public stickersApiService: StickersApiService,
              public store: AngularFirestore) {
    super(store);
  }

  startBackup() {
    if (this.stickersPack.length <= 0) {
      alert('Нет стикеров для загрузки');
      return
    }
    this.logs = [];
    this.successCount = 0;
    this.failCount = 0;
    this.cancel = false;
    this.update.nativeElement.disabled = true;
    const stickersCount = this.getStickersCount();
    this.addLog('Обновление данных. Дождитесь окончания');
    setTimeout(() => {
      if (this.cancel) {
        this.addLog('Обновление отменено');
        this.update.nativeElement.disabled = false;
        this.reset = false;
        return
      }
      if (this.reset) {
        for (let p = 0; p < this.stickersPack.length; p++) {
          if (this.stickersPack[p].stickerSet) {
            for (let s = 0; s < this.stickersPack[p].stickerSet.stickers.length; s++) {
              this.stickersPack[p].stickerSet.stickers[s].lastBackup = 0;
            }
          }
        }
      }
      this.loadSticker(1, stickersCount, 0, 0);
    }, 2000);
  }

  loadSticker(current, count, indexPack, indexSticker) {
    if (this.cancel) {
      this.addLog('Обновление отменено');
      this.update.nativeElement.disabled = false;
      this.reset = false;
      return
    }
    if (!this.stickersPack[indexPack].stickerSet) {
      current--;
      this.nextSticker(current, count, indexPack, indexSticker);
      return
    }
    const name = this.stickersPack[indexPack].stickerSet.name;
    const sticker = this.stickersPack[indexPack].stickerSet.stickers[indexSticker];
    if (sticker.lastBackup > 0) {
      this.addLog(`${current} из ${count}. Не требует обновления ~/${name}/${sticker.file_id}.webp`);
      this.nextSticker(current, count, indexPack, indexSticker);
      return
    }
    const subscription1 = this.stickersApiService.getFile(sticker)
      .pipe(
        mergeMap((response: any) => {
          if (response) {
            return this.stickersApiService.downloadFile(response.result.file_path);
          } else {
            return of(null);
          }
        })
      ).subscribe((response: Blob) => {
        if (response != null) {
          const stickerPath = `stickers-backup/${name}/${sticker.file_id}.webp`;
          const subscription2 = this.storage.ref(stickerPath).put(response)
            .snapshotChanges()
            .pipe(
              finalize(() => {
                this.successCount++;
                sticker.lastBackup = Date.now();
                this.addLog(`${current} из ${count}. Загружен стикер ~/${name}/${sticker.file_id}.webp`);
                this.nextSticker(current, count, indexPack, indexSticker);
                if (subscription2) {
                  subscription2.unsubscribe()
                }
              }),
              catchError(() => {
                this.failCount++;
                sticker.lastBackup = 0;
                this.addLog(`${current} из ${count}. Не удалось загрузить стикер ~/${name}/${sticker.file_id}.webp`);
                this.nextSticker(current, count, indexPack, indexSticker);
                if (subscription2) {
                  subscription2.unsubscribe()
                }
                return of(null);
              })
            )
            .subscribe();
          if (subscription1) {
            subscription1.unsubscribe()
          }
        } else {
          this.failCount++;
          sticker.lastBackup = 0;
          this.addLog(`${current} из ${count}. Не удалось скачать стикер ~/${name}/${sticker.file_id}.webp`);
          this.nextSticker(current, count, indexPack, indexSticker);
          if (subscription1) {
            subscription1.unsubscribe()
          }
        }
      });
  }

  nextSticker(current, count, indexPack, indexSticker) {
    current++;
    indexSticker++;
    if (this.stickersPack[indexPack].stickerSet && indexSticker < this.stickersPack[indexPack].stickerSet.stickers.length) {
      this.loadSticker(current, count, indexPack, indexSticker);
    } else {
      this.updateStickerPack(current, count, indexPack, indexSticker);
    }
  }

  nextStickerPack(current, count, indexPack, indexSticker) {
    indexSticker = 0;
    do {
      indexPack++;
    } while (indexPack < this.stickersPack.length && this.stickersPack[indexPack].stickerSet && this.stickersPack[indexPack].stickerSet.stickers.length <= 0);
    if (indexPack < this.stickersPack.length) {
      this.loadSticker(current, count, indexPack, indexSticker);
    } else {
      this.finishBackup();
    }
  }

  updateStickerPack(current, count, indexPack, indexSticker) {
    const title = this.stickersPack[indexPack].title;
    const name = !this.stickersPack[indexPack].stickerSet ? title : `${title} ~/${ this.stickersPack[indexPack].stickerSet.name}/*.webp`;
    const stickerPack = JSON.parse(JSON.stringify(this.stickersPack[indexPack]));
    delete stickerPack._uid;
    this.updateDocument('stickers-pack', this.stickersPack[indexPack]._uid, stickerPack, false)
      .then(() => {
        this.addLog(`Обновлена информация о стикерпаках ${name}`);
        this.nextStickerPack(current, count, indexPack, indexSticker);
      })
      .catch(() => {
        this.addLog(`Не удалось обновить информацию о стикерпаках ${name}`);
        this.nextStickerPack(current, count, indexPack, indexSticker);
      });
  }

  finishBackup() {
    this.addLog(`Обновлено ${this.successCount} стикеров. Не удалось обновить ${this.failCount} стикеров`);
    const date = new Date();
    this.infoApp.stickersBackupDate = ('0' + date.getDate()).slice(-2) + "." + ('0' + (date.getMonth() + 1)).slice(-2) + "." + date.getFullYear();
    this.updateDocument('info', 'app', this.infoApp, false)
      .then(() => {
        this.addLog(`Информация об обновлении сохранена`);
        this.addLog(`Готово`);
        this.update.nativeElement.disabled = false;
        this.reset = false;
      })
      .catch(() => {
        this.addLog(`Не удалось сохранить информацию об обновлении`);
        this.addLog(`Готово`);
        this.update.nativeElement.disabled = false;
        this.reset = false;
      });
  }

  cancelBackup() {
    this.cancel = true;
  }

  addLog(message) {
    this.logs.push(message);
  }

  getStickersCount(): number {
    let count = 0;
    for (let p = 0; p < this.stickersPack.length; p++) {
      if (this.stickersPack[p].stickerSet) {
        count += this.stickersPack[p].stickerSet.stickers.length
      }
    }
    return count;
  }
}
