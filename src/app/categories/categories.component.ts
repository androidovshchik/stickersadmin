import {Component, Input} from '@angular/core';
import {Category} from '../app.models';
import {FirestoreComponent} from '../firestore/firestore.component';
import {AngularFirestore} from '@angular/fire/firestore';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent extends FirestoreComponent {

  @Input()
  categories: Category[] = [];

  constructor(public store: AngularFirestore) {
    super(store);
  }

  addCategory(target) {
    if (!target.value.trim()) {
      target.value = "";
      return
    }
    target.disabled = true;
    const category: Category = {
      title: target.value.trim(),
      titleEn: null
    };
    const uid = CategoriesComponent.generateUid();
    this.updateDocument('categories', uid, category, false)
      .then(() => {
        category._uid = uid;
        this.categories.push(category);
        this.sortCategories();
        target.value = "";
        target.disabled = false;
        target.focus();
      })
      .catch(() => {
        target.disabled = false;
      });
  }

  updateCategory(target, category: Category) {
    if (!target.value.trim()) {
      target.value = "";
      return
    }
    target.disabled = true;
    const copy = JSON.parse(JSON.stringify(category));
    delete copy._uid;
    this.updateDocument('categories', category._uid, copy, false)
      .then(() => {
        target.disabled = false;
      })
      .catch(() => {
        target.disabled = false;
        alert('Не удалось добавить перевод для категории');
      });
  }

  deleteCategory(i, category: Category) {
    this.deleteDocument('categories', category._uid)
      .then(() => {
        this.categories.splice(i, 1)
      })
      .catch(() => {
        alert('Не удалось удалить категорию');
      });
  }

  sortCategories() {
    this.categories.sort((category1, category2) => {
      return category1.title.toLowerCase().localeCompare(category2.title.toLowerCase());
    });
  }
}
