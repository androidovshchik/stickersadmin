import {Routes} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {AuthUser} from './auth/user.guard';
import {DashboardComponent} from './dashboard/dashboard.component';

export const APP_ROUTES: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'tg-stickers',
    component: DashboardComponent,
    canActivate: [AuthUser]
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];
