import {Injectable} from '@angular/core';
import {ReplaySubject,} from 'rxjs';
import {StickerPack} from '../app.models';
import {FirestoreService} from './firestore.service';
import {AngularFirestore} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class StickerPackService extends FirestoreService {

  _items = new ReplaySubject<StickerPack>();

  constructor(public store: AngularFirestore) {
    super();
  }

  loadPart(previous?) {
    this._spinner.next(true);
    const subscription = this.store.collection<StickerPack>('stickers-pack', ref => {
      const query = ref.limit(FirestoreService.LIMIT)
        .orderBy('title');
      if (previous) {
        return query.startAfter(previous);
      }
      return query;
    }).snapshotChanges(null)
      .first()
      .subscribe((models: any[]) => {
        this.onSuccess('Sticker-pack', models.length);
        models.forEach(model => {
          this._items.next({
            _uid: model.payload.doc.id,
            ...model.payload.doc.data()
          });
        });
        if (models.length >= FirestoreService.LIMIT) {
          this.loadPart(models[models.length - 1].payload.doc);
        } else {
          this._items.next(null);
          this._spinner.next(false);
        }
        if (subscription) {
          subscription.unsubscribe();
        }
      }, error => {
        this._items.next(null);
        this.onError(error, 'Не удалось загрузить стикерпаки');
        if (subscription) {
          subscription.unsubscribe();
        }
      });
  }
}
