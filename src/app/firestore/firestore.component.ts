import {AngularFirestore} from '@angular/fire/firestore';

export class FirestoreComponent {

  constructor(public store: AngularFirestore) {}

  updateDocument(collection: string, document: string, object: object, merge: boolean): Promise<void> {
    console.log('Updating document ' + document);
    console.log(object);
    return this.store.collection(collection)
      .doc(document).ref
      .set(object, {merge: merge});
  }

  deleteDocument(collection: string, document: string): Promise<void> {
    console.log('Deleting document ' + document);
    return this.store.collection(collection)
      .doc(document)
      .delete();
  }

  static processSavedColumns(cell, displayedColumns: any[]): object {
    let object = {};
    for (let d = 0; d < displayedColumns.length; d++) {
      let key = displayedColumns[d];
      object[key] = FirestoreComponent.processValue(cell[key]);
    }
    return object;
  }

  static processValue(value) {
    if (/^ *$/.test(value)) {
      return null;
    } else if (Object.prototype.toString.call(value) === "[object Date]") {
      return isNaN(value.getTime()) ? null : +value.getTime();
    } else {
      return value;
    }
  }

  static isEmptyObject(object) {
    return Object.keys(object).every((key) => object[key] === null);
  }

  static generateUid(length: number = 24): string {
    // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789
    const chars = "3YoZ9kGPFQOmVW4iKpwcC7BqhydvHsgjUnflaMRzt6LeN8E5XDb2ux1TIASJr0";
    let uid = "";
    for (let i = 0; i < length; i++) {
      uid += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return uid;
  }
}
