import {Injectable} from '@angular/core';
import {ReplaySubject} from 'rxjs';
import {AngularFirestore} from '@angular/fire/firestore';
import {FirestoreService} from './firestore.service';
import {Category} from '../app.models';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService extends FirestoreService {

  _items = new ReplaySubject<Category>();

  constructor(public store: AngularFirestore) {
    super();
  }

  loadPart(previous?) {
    this._spinner.next(true);
    const subscription = this.store.collection<Category>('categories', ref => {
      const query = ref.limit(FirestoreService.LIMIT)
        .orderBy('title');
      if (previous) {
        return query.startAfter(previous);
      }
      return query;
    }).snapshotChanges(null)
      .first()
      .subscribe((models: any[]) => {
        this.onSuccess('Category', models.length);
        models.forEach(model => {
          this._items.next({
            _uid: model.payload.doc.id,
            ...model.payload.doc.data()
          });
        });
        if (models.length >= FirestoreService.LIMIT) {
          this.loadPart(models[models.length - 1].payload.doc);
        } else {
          this._items.next(null);
          this._spinner.next(false);
        }
        if (subscription) {
          subscription.unsubscribe();
        }
      }, error => {
        this._items.next(null);
        this.onError(error, 'Не удалось загрузить категории');
        if (subscription) {
          subscription.unsubscribe();
        }
      });
  }
}
