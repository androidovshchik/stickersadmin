import {BehaviorSubject, Subject} from 'rxjs';

export class FirestoreService {

    static LIMIT = 250;

    _spinner = new BehaviorSubject<boolean>(false);

    _errors = new Subject<string>();

    onSuccess(name: string, length: number) {
        console.log(name + '\'s length ' + length);
        this._spinner.next(true);
    }

    onError(error, message: string) {
        console.log(error);
        this._spinner.next(false);
        this._errors.next(message);
    }
}
