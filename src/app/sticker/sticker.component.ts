import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {mergeMap} from 'rxjs/operators';
import {of, Subscription} from 'rxjs';
import {StickersApiService} from '../stickers/stickers-api.service';
import {DomSanitizer} from '@angular/platform-browser';
import {Sticker} from '../app.models';

@Component({
  selector: 'app-sticker',
  templateUrl: './sticker.component.html',
  styleUrls: ['./sticker.component.css']
})
export class StickerComponent implements OnInit, OnDestroy {

  @Input()
  sticker: Sticker = {
    file_id: null,
    file_size: 0,
    width: 0,
    height: 0,
    lastBackup: 0,
    thumb: null,
    emoji: null,
    set_name: null,
    mask_position: null
  };

  subscription: Subscription;

  image: any;

  constructor(public stickersApiService: StickersApiService, private sanitizer: DomSanitizer) {}

  ngOnInit() {
    this.subscription = this.stickersApiService.getFile(this.sticker)
      .pipe(
        mergeMap((response: any) => {
          if (response) {
            return this.stickersApiService.downloadFile(response.result.file_path);
          } else {
            return of(null);
          }
        })
      ).subscribe((response: Blob) => {
        if (response != null) {
          this.image = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(response));
        }
      });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe()
    }
  }
}
