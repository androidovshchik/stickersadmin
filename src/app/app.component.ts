import {Component, OnInit} from '@angular/core';
import {CategoriesService} from './firestore/categories.service';
import {StickerPackService} from './firestore/sticker-pack.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(public categoriesService: CategoriesService, public stickersPackService: StickerPackService) {}

  ngOnInit() {
    this.categoriesService.loadPart(null);
    this.stickersPackService.loadPart(null);
  }
}
