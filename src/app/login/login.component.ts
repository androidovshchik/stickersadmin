import {Component, OnInit} from '@angular/core';
import {Admin} from '../app.models';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import {map} from 'rxjs/operators';
import * as firebase from 'firebase/app';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  admins: Admin[] = [];

  constructor(public router: Router, public fireAuth: AngularFireAuth, public store: AngularFirestore) {}

  ngOnInit() {
    let subscription = this.store.collection<Admin>('admins', ref => {
      return ref;
    }).snapshotChanges(null)
      .pipe(
        map(actions => {
          return actions.map(action => {
            return {
              _uid: action.payload.doc.id,
              ...action.payload.doc.data()
            };
          });
        })
      )
      .subscribe((models: Admin[]) => {
        console.log(models);
        models.forEach(model => {
          this.admins.push(model);
        });
        if (subscription) {
          subscription.unsubscribe()
        }
      }, error => {
        console.log(error);
        alert('Не удалось получить список пользователей');
        if (subscription) {
          subscription.unsubscribe()
        }
      });
  }

  login() {
    this.fireAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(result => {
        for (let a = 0; a < this.admins.length; a++) {
          if (this.admins[a]._uid == result.user.uid) {
            this.router.navigate(['tg-stickers'])
              .catch(error => {
                console.log(error);
              });
            return
          }
        }
        alert('Текущий пользователь не имеет доступ');
      })
      .catch(error => {
        console.log(error);
        alert('Не удалось войти');
      });
  }
}
