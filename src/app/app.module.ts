import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {firebase} from '../environments/firebase';
import {AppComponent} from './app.component';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {HttpClientModule} from '@angular/common/http';
import {CommonModule, LocationStrategy, PathLocationStrategy} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TagInputModule} from 'ngx-chips';
import {CategoriesComponent} from './categories/categories.component';
import {StickersComponent} from './stickers/stickers.component';
import {SettingsComponent} from './settings/settings.component';
import {StickerComponent} from './sticker/sticker.component';
import {LoginComponent} from './login/login.component';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {DashboardComponent} from './dashboard/dashboard.component';
import {RouterModule} from '@angular/router';
import {APP_ROUTES} from './app.routes';
import {AngularFireStorageModule} from '@angular/fire/storage';
import {BackupComponent} from './backup/backup.component';
import {NgxAutoScrollModule} from 'ngx-auto-scroll';

@NgModule({
  declarations: [
    AppComponent,
    CategoriesComponent,
    StickersComponent,
    SettingsComponent,
    StickerComponent,
    LoginComponent,
    DashboardComponent,
    BackupComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    TagInputModule,
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    NgxAutoScrollModule,
    RouterModule.forRoot(APP_ROUTES),
    AngularFireModule.initializeApp(firebase),
    AngularFireAuthModule,
    AngularFireStorageModule,
    AngularFirestoreModule
  ],
  providers: [
    {provide: LocationStrategy, useClass: PathLocationStrategy}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
