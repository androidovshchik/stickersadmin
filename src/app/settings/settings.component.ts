import {Component, Input} from '@angular/core';
import {FirestoreComponent} from '../firestore/firestore.component';
import {AngularFirestore} from '@angular/fire/firestore';
import {InfoApp} from '../app.models';
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent extends FirestoreComponent {

  @Input()
  infoApp: InfoApp = {
    botToken: null,
    proxyHost: null,
    proxyPort: null,
    proxyUsername: null,
    proxyPassword: null,
    stickersBackupDate: null
  };

  constructor(public router: Router, public store: AngularFirestore, public fireAuth: AngularFireAuth) {
    super(store);
  }

  // noinspection JSMethodCanBeStatic
  saveInfo(target) {
    target.innerText = 'Подождите...';
    this.updateDocument('info', 'app', this.infoApp, false)
      .then(() => {
        target.innerText = 'Сохранено';
        setTimeout(() => {
          target.innerText = 'Сохранить';
          target.disabled = false;
        }, 1000);
      })
      .catch(error => {
        console.log(error);
        target.innerText = 'Не сохранено';
        setTimeout(() => {
          target.innerText = 'Сохранить';
          target.disabled = false;
        }, 1000);
      });
  }

  logout() {
    this.fireAuth.auth.signOut()
      .then(() => {
        this.router.navigate(['login'])
          .catch(error => {
            console.log(error);
          });
      })
      .catch(error => {
        console.log(error);
        alert('Не удалось выйти');
      });
  }
}
