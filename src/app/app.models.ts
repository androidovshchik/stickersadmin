export interface InfoApp {
  botToken: string;
  proxyHost: string;
  proxyPort: string;
  proxyUsername: string;
  proxyPassword: string;
  stickersBackupDate: string;
}

export interface Document {
  _uid?: string;
}

export interface Admin extends Document {}

export interface Category extends Document {
  title: string;
  titleEn: string;
}

export interface StickerPack extends Document {
  title: string;
  titleEn: string;
  category: string;
  preview: string;
  stickerSet?: StickerSet;
  keywords: string[];
}

export interface StickerSet {
  name: string;
  title: string;
  contains_masks: boolean;
  stickers: Sticker[];
}

export interface Sticker {
  file_id: string;
  file_size?: number;
  width: number;
  height: number;
  lastBackup: number;
  thumb?: PhotoSize;
  emoji?: string;
  set_name?: string;
  mask_position?: MaskPosition;
}

export interface PhotoSize {
  file_id: string;
  file_size?: number;
  width: number;
  height: number;
}

export interface MaskPosition {
  point: string;
  x_shift: number;
  y_shift: number;
  scale: number;
}
